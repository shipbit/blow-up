extends RigidDynamicBody2D

@export var speed: int = 500
@export var fire_rate: float = 0.2
@export var strength = 20
@onready var sprite = $Sprite


func shoot(angle):
	apply_impulse(Vector2(speed, 0).rotated(angle - deg2rad(90)))
	


func _on_projectile_body_entered(body):
	if !body.is_in_group("player"):
		sprite.play("explode")
		if body.has_method("damage"):
			body.damage(strength, global_position)


func _on_sprite_animation_finished():
	if sprite.animation == 'explode'	:
		queue_free()
