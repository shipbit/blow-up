extends "res://scenes/entity/entity_base.gd"

@export var rotation_speed = 1.5
@onready var bulletPosition = $BulletPosition
var fireball: PackedScene = preload("res://scenes/player/fireball.tscn")
var in_hit_recovery = false

func _process(delta):
	if Input.is_action_pressed("primary_weapon") and !in_hit_recovery:
		var bullet = fireball.instantiate()
		bullet.position = bulletPosition.global_position
		bullet.rotation = rotation
		bullet.shoot(rotation)
		get_tree().get_root().add_child(bullet)
		in_hit_recovery = true
		await get_tree().create_timer(bullet.fire_rate).timeout
		in_hit_recovery = false
		
func _physics_process(delta):
	var direction = get_input_direction()
	velocity = Vector2.ZERO
	
	if direction != Vector2.ZERO:
		velocity += transform.y * direction.y * SPEED
		rotation += direction.x * rotation_speed * delta
		sprite.play("move")
	else:
		sprite.play("idle")
		
	move()
	
		
func get_input_direction():
	var input_direction = Vector2.ZERO
	
	input_direction.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_direction.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	
	return input_direction
