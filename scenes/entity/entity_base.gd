extends CharacterBody2D

@export var hp_max = 100
@export var hp = 100
@export var defense = 0

@export var SPEED = 75 

@onready var collisionShape = $CollisionShape
@onready var sprite = $Sprite
@onready var statChange = $StatChange

var popup = preload("res://scenes/damag_label.tscn")

func _physics_process(delta):
	move()
	
func move():
	move_and_slide()

func die():
	queue_free()
	
func damage(attack: int, position: Vector2):
	var dmg = attack * 100 / (100 + defense)
	var dmgLabel = popup.instantiate()
	dmgLabel.position = position
	dmgLabel.text = str(dmg,' !') 
	get_tree().get_root().add_child(dmgLabel)
	
	hp = max(0, hp - dmg)
	if hp == 0:
		die()
	

